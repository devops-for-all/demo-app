#!/bin/sh
envsubst '$${VAPORMAP_URL_SERVERNAME},$${VAPORMAP_URL_PORT},$${VAPORMAP_FRONTEND_ROOT}' < nginx.conf.template > vapormap.conf ;\
envsubst '$${VAPORMAP_BACKEND},$${VAPORMAP_BACKEND_PORT}' < config.json.template > config.json ;\
cp vapormap.conf /etc/nginx/conf.d ;\
cp -r . /usr/share/nginx/html ;\
nginx -g 'daemon off;'