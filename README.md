# Application configuration

## Description


Database:
 * MariaDB database

API:
 * Utilizes Django REST Framework
 * Interrogation via HTTP requests.
 * Connects to the database.
 * Exposes the endpoint "/api"

Frontend:
 * Utilizes the Nginx web server.
 * Static application.
 * Sends requests to the API from the user's browser.


## Frontend

- **Nginx server configuration**

The nginx server needs vapormap.conf file and the static files of the application:

- The configration file of nginx:

```yaml
server {
        listen 80;

        server_name  0.0.0.0;

        root frontend/;

        index index.html index.htm;

        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                try_files $uri $uri/ =404;
        }
        location = /favicon.ico { access_log off; log_not_found off; }

      }
```

Using environment variables:

```yaml
server {
        listen ${VAPORMAP_URL_PORT};
        server_name  ${VAPORMAP_URL_SERVERNAME};

        root ${VAPORMAP_FRONTEND_ROOT};

        index index.html index.htm;

        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                try_files $uri $uri/ =404;
                
        location = /favicon.ico { access_log off; log_not_found off; }

        }
      }
```

```console
export VAPORMAP_URL_SERVERNAME="****"
export VAPORMAP_URL_PORT="****"
export VAPORMAP_FRONTEND_ROOT="****"
```

Apply env variables to nginx configuration file

```console
envsubst '${VAPORMAP_URL_SERVERNAME},${VAPORMAP_URL_PORT},${VAPORMAP_FRONTEND_ROOT}' < nginx.conf.template > vapormap.conf
```

```console
cp vapormap.conf /etc/nginx/confd
cp -r frontend/* /usr/share/nginx/html
```

- Static files

```console
cp -r frontend/* /usr/share/nginx/html
```

nginx docs (to go further)
https://hub.docker.com/_/nginx
https://nginx.org/en/docs/beginners_guide.html#static

- **Configure backend url**

```bash
export VAPORMAP_BACKEND="****"
export VAPORMAP_BACKEND_PORT="****"
```

config.json file is used by the application to set the url of the backend

```json
{ "apiUrl": "http://${VAPORMAP_BACKEND}:${VAPORMAP_BACKEND_PORT}" }
```

```console
envsubst '${VAPORMAP_BACKEND},${VAPORMAP_BACKEND_PORT}' < config.json.template > config.json
```


## Backend configuration

- **Database:**

The backend is expecting a mysql 10.6 database with this configuration:

  - database name : "db_vapormap"
  - database user: "user_vapormap"
  - user password: "*******"

Check the database configuration

```bash
docker exec -it database mysql -D db_vapormap -u user_vapormap -p

mysql -D db_vapormap -u user_vapormap -p
```
---

## Deploy API server

Python version used 3.8

``` bash
cd app
pip install -r requirements/requirement.txt
```

### Configuration de la base de données 

Déclaration des variables de configuration pour la BDD et `migrate`

``` bash
export VAPORMAP_DBNAME=db_vapormap
export VAPORMAP_DBUSER=user_vapormap
export VAPORMAP_DBPASS=****
export VAPORMAP_DBHOST=***
export DJANGO_SETTINGS_MODULE="vapormap.settings.production"
```

Initializing the database (migrate):

``` bash
python manage.py makemigrations
python manage.py migrate
```

Launch the gunicorn server:

```bash
gunicorn vapormap.wsgi:application --bind 0.0.0.0:8001
```

